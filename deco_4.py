from functools import partial, update_wrapper


class Decorator:
    def __init__(self, func, name):
        self._func = func
        self._method_owner = name
        self.message = f"I'm decorating class {name}"

    def __call__(self, instance, owner):
        print(self.message)
        self._func(instance)

    def __get__(self, instance, owner):
        return partial(self.__call__, instance, owner)


class PersistentDecoMeta(type):
    def __init__(self, **kwargs):
        pass

    def __new__(cls, **kwargs):
        return super().__new__(cls, "PersistentDeco", (PersistentDeco,), kwargs)

class PersistentDeco(type):

    def __new__(cls, name, bases, namespace):
        new_class = super().__new__(cls, name, bases, namespace)
        for method_name, deco_class in cls.decorated_methods.items():
            decorated_method = deco_class(getattr(new_class, method_name), name)
            setattr(new_class, method_name, decorated_method)
        return new_class



class ParentClass(metaclass=PersistentDecoMeta(parent_method=Decorator)):
    def parent_method(self):
        pass


class ChildClass(ParentClass):
    def child_method(self):
        self.parent_method()


if __name__ == "__main__":
    parent_object = ParentClass()
    child_object = ChildClass()
    parent_object.parent_method()
    child_object.child_method()
