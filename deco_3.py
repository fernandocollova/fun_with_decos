class Decorator:
    def __init__(self, func, name):
        self._func = func
        self._method_owner = name

    def __call__(self, *args, **kwargs):
        print(f"I'm decorating class {self._method_owner}")


class PersistentDecoBase:
    def __new__(cls, *args, **kwargs):
        for key, value in cls._decorated_methods.items():
            original_method = getattr(cls, key)
            decorated_method = value(original_method, cls.__name__)
            setattr(cls, key, decorated_method)
        return super().__new__(cls)


def persistent_deco(**kwargs):
    return type(
        "PersistentDeco",
        (PersistentDecoBase,),
        {"_decorated_methods": kwargs},
    )

BaseClass = persistent_deco(
    parent_method=Decorator,
)

class ParentClass(BaseClass):
    def parent_method(self):
        pass


class ChildClass(ParentClass):
    def child_method(self):
        self.parent_method()


if __name__ == "__main__":
    parent_object = ParentClass()
    child_object = ChildClass()
    parent_object.parent_method()
    child_object.child_method()
