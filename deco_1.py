def _extract_class_name_from_method(method) -> str:
    return str(method.__qualname__).split(".")[0]


class Decorator:
    def __init__(self, func):
        self._func = func
        self._method_owner = _extract_class_name_from_method(func)

    def __call__(self, *args, **kwargs):
        print(f"I'm decorating class {self._method_owner}")


class ParentClass:
    @Decorator
    def parent_method(self):
        pass


class ChildClass(ParentClass):
    def child_method(self):
        self.parent_method()


if __name__ == "__main__":
    parent_object = ParentClass()
    child_object = ChildClass()
    parent_object.parent_method()
    child_object.child_method()

