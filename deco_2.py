class Decorator:
    def __init__(self, func, classname):
        self._func = func
        self._method_owner = classname

    def __call__(self, *args, **kwargs):
        print(f"I'm decorating class {self._method_owner}")


class AutoDecorate:
    autodecorate = ("parent_method",)

    def __new__(cls, *args, **kwargs):
        for item in cls.autodecorate:
            original_method = getattr(cls, item)
            decorated_method = Decorator(original_method, cls.__name__)
            setattr(cls, item, decorated_method)
        return super().__new__(cls)


class ParentClass(AutoDecorate):
    def parent_method(self):
        pass


class ChildClass(ParentClass):
    def child_method(self):
        self.parent_method()


if __name__ == "__main__":
    parent_object = ParentClass()
    child_object = ChildClass()
    parent_object.parent_method()
    child_object.child_method()
