import sqlite3
from time import sleep

CONN_DATA = "./rarbg_db.sqlite"


def get_conn(conn_data):
    return sqlite3.connect(conn_data)


class MakeUppercase():
    def __init__(self, only_first_leter=True):
        self.only_first_leter = only_first_leter

    def transform(self, data):
        """Transform data inplace"""
        sleep(10)

class CastToInt:
    def __init__(self, columns=()):
        self.columns = columns

    def transform(self, data):
        """Transform data inplace"""
        sleep(10)

class DataExtractor:
    def __init__(self, sql):
        self.sql = sql

    def extract(self, conn_data):
        with get_conn(conn_data) as conn:
            with conn.cursor as cur:
                return list(cur.execute(self.sql))

class Transformer:
    def __init__(self, transformers):
        self.transformers = transformers

    def transform(self, data):
        for trasnformer in self.transformers:
            trasnformer(data)


def main():
    extractor = DataExtractor("SELECT * FROM items")
    data = extractor.extract(CONN_DATA)
    optimus_prime = Transformer(transformers=[MakeUppercase(), CastToInt()])
    optimus_prime.transform(data)


##################################################################################

def databse_connection(conn_data):
    def f():
        return sqlite3.connect(conn_data)
    return f


def transform(*args):
    def f(data):
        for transformer in args:
            transformer.transform(data)
    return f


def extract(sql, conn):
    def f():
        with conn() as conn:
            with conn.cursor() as cur:
                return conn.execute(sql)
    return f


def extract_and_transform(extractor, transformer):
    def f():
        transformer(extractor())
    return f


def get_operations():
    get_extraction_connection = databse_connection(CONN_DATA)
    transformers_for_whatever = transform(MakeUppercase(), CastToInt())
    extractor = extract("SELECT * FROM items", get_extraction_connection)
    return extract_and_transform(extractor, transformers_for_whatever)


work = get_operations()
