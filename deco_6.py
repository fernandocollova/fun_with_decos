from functools import partial, update_wrapper


class DecoMeta(type):
    def __get__(self, instance, owner):
        print(self, instance, owner)
        return super().__get__(instance, owner)
        # return partial(self.__call__, instance, owner)


class Decorator(metaclass=DecoMeta):
    def __init__(self, func):
        self._func = func
        self.message = f"I'm decorating class {func}"

    def __call__(self, instance, owner):
        print(self.message)
        self._func(instance)

    def __get__(self, instance, owner):
        return partial(self.__call__, instance, owner)


class ParentClass():
    @Decorator
    def parent_method(self):
        pass


class ChildClass(ParentClass):
    def child_method(self):
        self.parent_method()


if __name__ == "__main__":
    parent_object = ParentClass()
    child_object = ChildClass()
    parent_object.parent_method()
    child_object.child_method()
