from functools import partial, update_wrapper
# https://stackoverflow.com/questions/3798835/understanding-get-and-set-and-python-descriptors

class DecoExecutor:
    def __init__(self, deco, instance, owner):
        self.deco = deco
        self.instance = instance
        self.owner = owner

    def __call__(self):
        print(f"I'm decorating mathod {self.owner.__name__}.{self.deco._func.__name__}")
        return self.deco._func(self.instance)


class Decorator:
    def __init__(self, func):
        self._func = func

    def __get__(self, instance, owner):
        return DecoExecutor(self, instance, owner)

    # # Option 2
    # def __call__(self, instance, owner):
    #     return self._func(instance)

    # def __get__(self, instance, owner):
    #     return partial(self.__call__, instance, owner)


class ParentClass():
    @Decorator
    def parent_method(self):
        pass


class ChildClass(ParentClass):
    def child_method(self):
        self.parent_method()


if __name__ == "__main__":
    parent_object = ParentClass()
    child_object = ChildClass()
    parent_object.parent_method()
    child_object.child_method()
